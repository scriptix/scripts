package main

import (
	"fmt"
	"time"
)

func main() {
	for i:=0;i<50;i++ {
		fmt.Println("Process number:",i)
		time.Sleep(1000 * time.Millisecond)
	}
}